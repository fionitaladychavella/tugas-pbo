package com.company;

// Kelas interface Diskon
interface Diskon {
    // Method abstract untuk menghitung diskon
    double hitungDiskon();
}
