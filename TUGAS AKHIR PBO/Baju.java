package com.company;

class Baju extends Barang implements Diskon {
    // Atribut
    public int jumlah;

    // Konstruktor
    public Baju(String nama, int harga, int jumlah) {
        super(nama, harga);
        this.jumlah = jumlah;
    }

    // Implementasi method abstract hitungTotalHarga dari kelas Barang
    @Override
    public int hitungTotalHarga() {
        return harga * jumlah;
    }
    // Implementasi method abstract hitungDiskon dari interface Diskon
    @Override
    public double hitungDiskon() {
        if (jumlah >= 3) {
            return 0.1;
        } else {
            return 0;
        }
    }
}