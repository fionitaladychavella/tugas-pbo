package com.company;

class Skincare extends Barang {
    // Atribut
    public int jumlah;
    public double diskon;

    // Konstruktor
    public Skincare(String nama, int harga, int jumlah, double diskon) {
        super(nama, harga);
        this.jumlah = jumlah;
        this.diskon = diskon;
    }

    // Implementasi method abstract hitungTotalHarga dari kelas Barang
    @Override
    public int hitungTotalHarga() {
        return (int) ((harga * jumlah) - (harga * jumlah * diskon));
    }
}
