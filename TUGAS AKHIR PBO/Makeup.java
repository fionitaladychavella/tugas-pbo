package com.company;

    // Kelas Makeup yang merupakan turunan dari kelas Barang
   public class Makeup extends Barang {
        // Atribut
        public int jumlah;

        // Konstruktor
        public Makeup(String nama, int harga, int jumlah) {
            super(nama, harga);
            this.jumlah = jumlah;
        }

        // Implementasi method abstract hitungTotalHarga dari kelas Barang
        @Override
        public int hitungTotalHarga() {
            return harga * jumlah;
        }
    }

