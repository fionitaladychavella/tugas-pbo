package com.company;

import java.util.Scanner;

// Kelas abstract Barang
abstract class Barang {
    // Atribut
    protected String nama;
    protected int harga;

    // Konstruktor
    public Barang(String nama, int harga) {
        this.nama = nama;
        this.harga = harga;
    }

    public Barang() {

    }


    // Method abstract untuk menghitung total harga
    public abstract int hitungTotalHarga();
}