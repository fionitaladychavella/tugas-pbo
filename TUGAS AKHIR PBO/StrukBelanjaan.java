package com.company;
import jdk.swing.interop.SwingInterOpUtils;

import java.util.Scanner;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

// Kelas StrukBelanjaan
public class StrukBelanjaan {
    void struk() throws IOException {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int totalHarga = 0;
        Barang[] daftarBarang = new Barang[100];
        int jumlahBarang = 0;
        do {
            System.out.println("=================================");
            System.out.println("|| Silahkan pilih menu berikut ||");
            System.out.println("=================================");
            System.out.println("|| Menu:                       ||");
            System.out.println("|| 1. Tambah barang            ||");
            System.out.println("|| 2. Cetak struk              ||");
            System.out.println("|| 3. Baca struk               ||");
            System.out.println("|| 4. Keluar                   ||");
            System.out.println("=================================");
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            System.out.print("  Pilihan: ");

            try {
                pilihan = input.nextInt();
                switch (pilihan) {
                    case 1:
                        System.out.println("=================================");
                        System.out.println("||         Tambah barang       ||");
                        System.out.println("=================================");
                        System.out.println("|| 1. Makeup                   ||");
                        System.out.println("|| 2. Skincare                 ||");
                        System.out.println("|| 3. Pakaian                  ||");
                        System.out.println("=================================");
                        System.out.print("Pilihan: ");
                        int pilihanTambah = input.nextInt();
                        switch (pilihanTambah) {
                            case 1:
                                System.out.println("=================================");
                                System.out.println("||           MAKE UP           ||");
                                System.out.println("=================================");
                                System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                                System.out.print("Masukkan nama barang: ");
                                String nama = input.next();
                                System.out.print("Masukkan harga barang: ");
                                int harga = input.nextInt();
                                System.out.print("Masukkan jumlah barang: ");
                                int jumlah = input.nextInt();
                                daftarBarang[jumlahBarang] = new Makeup(nama, harga, jumlah);
                                jumlahBarang++;
                                break;
                            case 2:
                                System.out.println("=================================");
                                System.out.println("||            SKINCARE         ||");
                                System.out.println("=================================");
                                System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                                System.out.print("Masukkan nama barang: ");
                                nama = input.next();
                                System.out.print("Masukkan harga barang: ");
                                harga = input.nextInt();
                                System.out.print("Masukkan jumlah barang: ");
                                jumlah = input.nextInt();
                                System.out.print("Masukkan persentase diskon: ");
                                double diskon = input.nextDouble();
                                daftarBarang[jumlahBarang] = new Skincare(nama, harga, jumlah, diskon);
                                jumlahBarang++;
                                break;
                            case 3:
                                System.out.println("=================================");
                                System.out.println("||            PAKAIAN          ||");
                                System.out.println("=================================");
                                System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                                System.out.print("Masukkan nama barang: ");
                                nama = input.next();
                                System.out.print("Masukkan harga barang: ");
                                harga = input.nextInt();
                                System.out.print("Masukkan jumlah barang: ");
                                jumlah = input.nextInt();
                                daftarBarang[jumlahBarang] = new Baju(nama, harga, jumlah);
                                jumlahBarang++;
                                break;
                        }
                        break;
                    case 2:
                        System.out.println("=================================");
                        System.out.println("||          CETAK STRUK        ||");
                        System.out.println("=================================");

// Buat output stream untuk file "struk.txt"
                        FileOutputStream fos = new FileOutputStream("struk.txt");
                        for (int i = 0; i < jumlahBarang; i++) {
                            Barang b = daftarBarang[i];
                            if (b instanceof Makeup) {
                                System.out.println("Nama barang: " + b.nama);
                                System.out.println("Harga barang: " + b.harga);
                                System.out.println("Jumlah barang: " + ((Makeup) b).jumlah);
// Tulis  informasi barang ke file

                                String data = "Nama barang: " + b.nama + "\n"
                                        + "Harga barang: " + b.harga + "\n"
                                        + "Jumlah barang: " + ((Makeup) b).jumlah + "\n";
                                fos.write(data.getBytes());
                            } else if (b instanceof Skincare) {
                                System.out.println("Nama barang: " + b.nama);
                                System.out.println("Harga barang: " + b.harga);
                                System.out.println("Jumlah barang: " + ((Skincare) b).jumlah);
                                System.out.println("Diskon: " + ((Skincare) b).diskon + "%");
// Tulis informasi barang ke file
                                String data = "Nama barang: " + b.nama + "\n"
                                        + "Harga barang: " + b.harga + "\n"
                                        + "Jumlah barang: " + ((Skincare) b).jumlah + "\n"
                                        + "Diskon: " + ((Skincare) b).diskon + "%" + "\n";
                                fos.write(data.getBytes());
                            } else if (b instanceof Baju) {
                                System.out.println("Nama barang: " + b.nama);
                                System.out.println("Harga barang: " + b.harga);
                                System.out.println("Jumlah barang: " + ((Baju) b).jumlah);
// Tulis informasi barang ke file
                                String data = "Nama barang: " + b.nama + "\n"
                                        + "Harga barang: " + b.harga + "\n"
                                        + "Jumlah barang: " + ((Baju) b).jumlah + "\n";
                                fos.write(data.getBytes());
                            }
                            totalHarga += b.harga;
                        }
// Tulis total harga ke file
                        String data = "Total harga: " + totalHarga + "\n";
                        fos.write(data.getBytes());
// Tutup output stream
                        fos.close();
                        break;
                    case 3:
                        System.out.println("Baca struk");
// Buat input stream untuk file "struk.txt"
                        FileInputStream fis = new FileInputStream("struk.txt");
// Baca isi file seluruhnya ke dalam array of bytes
                        data = String.valueOf(fis.readAllBytes());
// Ubah array of bytes menjadi string

                        String isiFile = new String(data);
// Cetak isi file ke console
                        System.out.println(isiFile);
// Tutup input stream
                        fis.close();
                        break;
                    case 4:
                        System.out.println("Keluar");
                        break;
                    default:
                        System.out.println("Pilihan tidak valid");
                        break;
                }
            } catch (Exception e) {
                System.out.println("Input tidak valid");
                input.next();
            }
        } while (pilihan != 4);
    }
}

